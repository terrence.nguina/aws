### Variables
variable front_instance_number {
  default = "2"
}

variable front_ami {
  default = "ami-0d77397e" # Ubuntu 16.04
}

variable front_instance_type {
  default = "t2.micro"
}

variable public_key {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAv8LB1uEDtsRwHzq6hnUzmHNUT5CGhgrhjHWpKRcGjgeMBDrbXxp6YNKR0MDKz47LcUQ3oizgfGuCKhu0Yg8IXBtysCXuOTeRqL6TksjqzkOE4xk8Kat+CVw/1eszHoVI/Zt9GybmBRI63GlZ3DRfRESDZ1Tlv/BCr45dgVgPlQH0MPLgdDIcBRe2UrrwWnpaWEMNixoV+aXFYhR2Pi65qeJoASUE9YZn+d3udD+ygSoPaYU4dfxHf0MY3hy+PCBSsFY/TrvhwTsU3zaqsPSFBCSqBqiO0P6CHv5RQgy98u+aP89E5QS4Es21MSvlfjFw4arTYRe8/hMgqK38vUZaTQ== rsa-key-20191018"
}

variable front_elb_port {
  default = "80"
}

variable front_elb_protocol {
  default = "http"
}

### Resources
resource "aws_key_pair" "front" {
  key_name   = "${var.project_name}-front"
  public_key = "${var.public_key}"
}

resource "aws_instance" "front" {
  # TO DO
  # see https://www.terraform.io/docs/providers/aws/r/instance.html
  count                  = "${var.front_instance_number}"
  ami                    = "${var.front_ami}"
  instance_type          = "${var.front_instance_type}"
  subnet_id              = "${aws_subnet.public.*.id[count.index]}"
  vpc_security_group_ids = ["${aws_security_group.front.id}"]
  key_name               = "${aws_key_pair.front.key_name}"

  tags = {
    Name = "${var.project_name}-front-${count.index}"
  }
}

resource "aws_elb" "front" {
  # TO DO
  # see https://www.terraform.io/docs/providers/aws/r/elb.html
  name            = "${var.project_name}-front-elb"
  subnets         = ["${aws_subnet.public.*.id}"]
  security_groups = ["${aws_security_group.elb.id}"]
  instances       = ["${aws_instance.front.*.id}"]

  listener {
    instance_port     = "${var.front_elb_port}"
    instance_protocol = "${var.front_elb_protocol}"
    lb_port           = "${var.front_elb_port}"
    lb_protocol       = "${var.front_elb_protocol}"
  }

  tags = {
    Name = "${var.project_name}-front-elb"
  }
}

### Outputs
output "elb_endpoint" {
  # TO DO
  # see https://www.terraform.io/intro/getting-started/outputs.html
  value = "${aws_elb.front.dns_name}"
}

output "instance_ip" {
  # TO DO
  value = ["${aws_instance.front.*.public_ip}"]
}
